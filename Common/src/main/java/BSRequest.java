import java.io.Serializable;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class BSRequest implements Serializable{
    RequestType requestType;
    String JsonString;

    public BSRequest(RequestType requestType) {
        this.requestType = requestType;
    }

    public BSRequest(RequestType requestType, String jsonString) {
        this.requestType = requestType;
        JsonString = jsonString;
    }

    @Override
    public String toString() {
        return "BSRequest{" +
                "requestType=" + requestType +
                ", JsonString='" + JsonString + '\'' +
                '}';
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public String getJsonString() {
        return JsonString;
    }
}
