package exceptions;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class ReservationNotPossibleException extends Exception {
    public ReservationNotPossibleException(String msg){
        super(msg);
    }
}
