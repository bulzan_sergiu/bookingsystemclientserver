package exceptions;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ReservationNotEnoughSeats extends Exception {
    public ReservationNotEnoughSeats(String msg){
        super(msg);
    }
}
