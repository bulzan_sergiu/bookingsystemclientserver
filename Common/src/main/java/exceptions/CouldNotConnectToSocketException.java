package exceptions;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class CouldNotConnectToSocketException extends Exception {
    public CouldNotConnectToSocketException(String msg){
        super(msg);
    }
}