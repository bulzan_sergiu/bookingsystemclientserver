package exceptions;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ExistingUserException extends Exception {
    public ExistingUserException(String msg){
        super(msg);
    }
}