package exceptions;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class SignInException extends Exception {
    public SignInException(String msg){
        super(msg);
    }
}

