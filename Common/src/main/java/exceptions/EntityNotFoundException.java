package exceptions;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(String msg){
        super(msg);
    }
}