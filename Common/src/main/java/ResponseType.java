/**
 * Created by sergiubulzan on 08/04/2017.
 */
public enum ResponseType {
    OK, ERROR, ReservationAdded, ReservationDeleted
}
