/**
 * Created by sergiubulzan on 08/04/2017.
 */
public enum RequestType {
    SignUp, SignIn,
    GetAllExcursions, GetAllReservations,
    CheckUpdates,
    GetExcursion,
    MakeReservation, RemoveReservation
}
