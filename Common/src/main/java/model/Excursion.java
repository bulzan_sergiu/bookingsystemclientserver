package model;

import java.io.Serializable;
import java.sql.Time;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class Excursion implements Serializable{
    private int excursionId;
    private Attraction attraction;
    private Firm firm;
    private Time departTime;
    private int price;
    private int availableSeats;


    public Excursion(int excursionId, Attraction attraction, Firm firm, Time departTime, int price, int availableSeats) {
        this.excursionId = excursionId;
        this.attraction = attraction;
        this.firm = firm;
        this.departTime = departTime;
        this.price = price;
        this.availableSeats = availableSeats;
    }



    public int getExcursionId() {
        return excursionId;
    }

    public void setExcursionId(int excursionId) {
        this.excursionId = excursionId;
    }

    public Attraction getAttraction() {
        return attraction;
    }

    public void setAttraction(Attraction attraction) {
        this.attraction = attraction;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Time getDepartTime() {
        return departTime;
    }

    public void setDepartTime(Time departTime) {
        this.departTime = departTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        model.Excursion excursion = (model.Excursion) o;

        if (excursionId != excursion.excursionId) return false;
        if (price != excursion.price) return false;
        if (availableSeats != excursion.availableSeats) return false;
        if (attraction != null ? !attraction.equals(excursion.attraction) : excursion.attraction != null) return false;
        if (firm != null ? !firm.equals(excursion.firm) : excursion.firm != null) return false;
        return departTime != null ? departTime.equals(excursion.departTime) : excursion.departTime == null;

    }

    @Override
    public int hashCode() {
        int result = excursionId;
        result = 31 * result + (attraction != null ? attraction.hashCode() : 0);
        result = 31 * result + (firm != null ? firm.hashCode() : 0);
        result = 31 * result + (departTime != null ? departTime.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + availableSeats;
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(excursionId);
    }
}
