package model;

import java.io.Serializable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class Attraction implements Serializable{
    private static final long serialVersionUID = 1;

    private int attrId;
    private String name;


    public Attraction(int attrId, String name) {
        this.attrId = attrId;
        this.name = name;
    }

    public int getAttrId() {
        return attrId;
    }

    public String getName() {
        return name;
    }

    public void setAttrId(int attrId) {
        this.attrId = attrId;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attraction that = (Attraction) o;

        if (attrId != that.attrId) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = attrId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }



}