package model;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class Reservation {
    Integer resId;
    Excursion excursion;
    String clientName;
    String phone;
    Integer tickets;

    Integer excursionId;

    public Integer getExcursionId() {
        return excursionId;
    }

    public void setExcursionId(Integer excursionId) {
        this.excursionId = excursionId;
    }

    public Reservation(){}

    public Reservation(Integer resId, Integer excursionId, String clientName, String phone, Integer tickets){
        this.resId = resId;
        this.excursionId = excursionId;
        this.clientName = clientName;
        this.phone = phone;
        this.tickets = tickets;
        this.excursion = null;
    }

    public Reservation(Excursion excursion, String clientName, String phone, Integer tickets) {
        this.resId = 0;
        this.excursion = excursion;
        this.clientName = clientName;
        this.phone = phone;
        this.tickets = tickets;
        this.excursionId = excursion.getExcursionId();
    }

    public Reservation(Integer resId, Excursion excursion, String clientName, String phone, Integer tickets) {
        this.resId = resId;
        this.excursion = excursion;
        this.clientName = clientName;
        this.phone = phone;
        this.tickets = tickets;
        this.excursionId = excursion.getExcursionId();
    }

    public Integer getResId() {
        return resId;
    }

    public void setResId(Integer resId) {
        this.resId = resId;
    }

    public Excursion getExcursion() {
        return excursion;
    }

    public void setExcursion(Excursion excursion) {
        this.excursion = excursion;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTickets() {
        return tickets;
    }

    public void setTickets(Integer tickets) {
        this.tickets = tickets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reservation that = (Reservation) o;

        if (resId != that.resId) return false;
        if (tickets != that.tickets) return false;
        if (excursion != null ? !excursion.equals(that.excursion) : that.excursion != null) return false;
        if (clientName != null ? !clientName.equals(that.clientName) : that.clientName != null) return false;
        return phone != null ? phone.equals(that.phone) : that.phone == null;

    }

    @Override
    public int hashCode() {
        int result = resId;
        result = 31 * result + (excursion != null ? excursion.hashCode() : 0);
        result = 31 * result + (clientName != null ? clientName.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + tickets;
        return result;
    }


    @Override
    public String toString() {
        return "Reservation{" +
                "resId=" + resId +
                ", excursion=" + excursion +
                ", clientName='" + clientName + '\'' +
                ", phone='" + phone + '\'' +
                ", tickets=" + tickets +
                '}';
    }
}
