package model;

import java.io.Serializable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class Firm implements Serializable{
    private int firmId;
    private String name;

    public Firm(int firmId, String name) {
        this.firmId = firmId;
        this.name = name;
    }

    public int getFirmId() {
        return firmId;
    }

    public void setFirmId(int firmId) {
        this.firmId = firmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Firm firm = (Firm) o;

        if (firmId != firm.firmId) return false;
        return name != null ? name.equals(firm.name) : firm.name == null;

    }

    @Override
    public int hashCode() {
        int result = firmId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }



    @Override
    public String toString() {
        return name;
    }
}
