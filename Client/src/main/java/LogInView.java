/**
 * Created by sergiubulzan on 09/04/2017.
 */

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import exceptions.ExistingUserException;
import exceptions.SignInException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.Users;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;






/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class LogInView {
    AbstractClientService clientService;


    BorderPane borderPane;
    private Button signIn;
    private Button signUp;
    private Button buttonExit;
    private Label mesaj;

    private TextField username;
    private PasswordField password;
    public Stage stage;

    public LogInView(Stage stage, AbstractClientService service){
        this.stage = stage;
        this.clientService = service;

        this.username = new TextField();
        this.username.setPromptText("username: ");

        this.password = new PasswordField();
        this.password.setPromptText("password: ");

        this.mesaj = new Label("");


        this.signIn = new Button("Sign In");
        this.signUp = new Button("Sign Up");


        this.buttonExit = new Button("Exit");

        this.initBorderPane();
    }

    public BorderPane getView(){
        return this.borderPane;
    }

    private void initBorderPane(){
        this.borderPane = new BorderPane();



        borderPane.setPadding(new Insets(10,20,10,20));

        VBox firstP = new VBox(new Label("username: "),username);
        VBox secondP = new VBox(new Label("password: "),password);




        HBox buttons = new HBox(10, signIn,signUp);

        VBox vboxCenter = new VBox(10,firstP,secondP,buttons,mesaj);



        this.borderPane.setCenter(vboxCenter);

        HBox hBox = new HBox(10,buttonExit);
        hBox.setAlignment(Pos.CENTER_RIGHT);


        this.signUp.setOnAction(event -> {
            signUp();
        });

        this.signIn.setOnAction(event -> {
            signIn();
        });



        this.buttonExit.setOnAction(event -> {
            Stage stage = (Stage) buttonExit.getScene().getWindow();
            // do what you have to do
            stage.close();
        });

        this.borderPane.setBottom(hBox);
    }

    public void signUp() {
        if (this.username.getText().isEmpty() || this.password.getText().isEmpty())
            warning("campuri goale! ");
        else{
            try{
                clientService.sendSignUpRequest(new Users(this.username.getText(), this.password.getText()));
                this.mesaj.setTextFill(Color.DARKGREEN);
                this.mesaj.setText("Inregistrare cu succes");
            }catch (ExistingUserException ex){
                this.mesaj.setTextFill(Color.DARKRED);
                this.mesaj.setText(ex.getMessage());
            }
        }
    }

    public void signIn(){
        if(this.username.getText().isEmpty() || this.password.getText().isEmpty())
            warning("campuri goale! ");
        else{
            try{
                clientService.sendSignInRequest(new Users(this.username.getText(), this.password.getText()));
                signInSuccessfull();
            }catch (SignInException ex){
                this.mesaj.setTextFill(Color.DARKRED);
                this.mesaj.setText(ex.getMessage());
            }
        }
    }

    private void warning(String war) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setContentText(war);
        alert.showAndWait();
    }

    public void signInSuccessfull() {

        stage.setTitle("Logat");

        MainClientView ofView = new MainClientView(clientService);
        Parent p = ofView.getView();

        stage.setScene(new Scene(p,1300,600));

        stage.show();
    }
}
