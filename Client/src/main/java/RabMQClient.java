import com.google.gson.Gson;
import com.rabbitmq.client.*;
import exceptions.EntityNotFoundException;
import exceptions.ExistingUserException;
import exceptions.ReservationNotPossibleException;
import exceptions.SignInException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Excursion;
import model.Reservation;
import model.Users;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;

/**
 * Created by sergiubulzan on 13/05/2017.
 */
public class RabMQClient extends AbstractClientService{

    private Connection connection;
    private Channel channel;
    private String requestQueueName = "bs_rpc_queue";
    private String replyQueueName;

    String corrId;
    private AMQP.BasicProperties props;


    public RabMQClient() throws IOException, TimeoutException{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        connection = factory.newConnection();
        channel = connection.createChannel();

        //the queue for recieving responses
        replyQueueName = channel.queueDeclare().getQueue();

        //identifier for current client
        corrId = UUID.randomUUID().toString();

        props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        gson = new Gson();
    }

    final BlockingQueue<Response> responses = new ArrayBlockingQueue<Response>(2);

    public Response handleRequest(BSRequest request){
        try{
            byte[] requestData = Serialization.serialize(request);

            channel.basicPublish("",requestQueueName,props,requestData);

            channel.basicConsume(replyQueueName,true, new DefaultConsumer(channel){
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    if (properties.getCorrelationId().equals(corrId)) {
                        try {
                            Response resp = (Response) Serialization.deserialize(body);
                            responses.offer(resp);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            //wait for a response
            Response resp = responses.take();
            return resp;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void run() {
        try {
            channel.exchangeDeclare("notifications", BuiltinExchangeType.DIRECT);
            String queueName = channel.queueDeclare().getQueue();

            channel.queueBind(queueName, "notifications", "info");

            Consumer consumer = new DefaultConsumer(channel){
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                                           AMQP.BasicProperties properties, byte[] body) throws IOException {
                    try {
                        Response resp = (Response) Serialization.deserialize(body);
                        System.out.println(" [x] Received notification '" + envelope.getRoutingKey() + "':'" + resp + "'");
                        updateChanges(resp);

                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            };

            channel.basicConsume(queueName, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
