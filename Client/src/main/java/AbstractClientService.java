import com.google.gson.Gson;
import exceptions.EntityNotFoundException;
import exceptions.ExistingUserException;
import exceptions.ReservationNotPossibleException;
import exceptions.SignInException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Excursion;
import model.Reservation;
import model.Users;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sergiubulzan on 14/05/2017.
 */
public abstract class AbstractClientService implements IClientService{

    protected MainClientView mainClientView;
    protected Gson gson = new Gson();

    public AbstractClientService(){}

    @Override
    public void setMainClientView(MainClientView mainClientView) {
        this.mainClientView = mainClientView;
    }

    @Override
    public boolean sendSignUpRequest(Users user) throws ExistingUserException {
        BSRequest bsRequest = new BSRequest(RequestType.SignUp,gson.toJson(user));
        Response response = handleRequest(bsRequest);
        System.out.println(response);
        if(response.getType() == ResponseType.ERROR){
            throw new ExistingUserException(response.getMessage());
        }
        return true;
    }

    @Override
    public boolean sendSignInRequest(Users user) throws SignInException {
        BSRequest bsRequest = new BSRequest(RequestType.SignIn, gson.toJson(user));
        Response response = handleRequest(bsRequest);
        if(response.getType() == ResponseType.ERROR)
            throw new SignInException(response.getMessage());
        return true;
    }

    @Override
    public ObservableList<Excursion> getAllExcursions() {
        BSRequest bsRequest = new BSRequest(RequestType.GetAllExcursions);
        Response response = handleRequest(bsRequest);

        String gsonex = response.getjSonResponse();
        System.out.println(gsonex);

        Excursion[] excursionsArray = gson.fromJson(response.getjSonResponse(),Excursion[].class);
        List<Excursion> excursions = Arrays.asList(excursionsArray);
        ObservableList<Excursion> obsExcurions= FXCollections.observableArrayList(excursions);

        return obsExcurions;
    }

    @Override
    public ObservableList<Reservation> getAllReservations() {
        BSRequest bsRequest = new BSRequest(RequestType.GetAllReservations);
        Response response = handleRequest(bsRequest);
        Reservation[] reservationsArray = gson.fromJson(response.getjSonResponse(),Reservation[].class);
        List<Reservation> reservations = Arrays.asList(reservationsArray);

        ObservableList<Reservation> obsReservations= FXCollections.observableArrayList(reservations);
        return obsReservations;
    }

    @Override
    public Excursion getExcursion(int id) throws EntityNotFoundException {
        BSRequest bsRequest = new BSRequest(RequestType.GetExcursion, String.valueOf(id));
        Response response = handleRequest(bsRequest);
        if(response.getType() == ResponseType.ERROR)
            throw new EntityNotFoundException(response.getMessage());
        Excursion exc = gson.fromJson(response.getjSonResponse(), Excursion.class);
        return exc;
    }

    @Override
    public void makeReservation(Reservation reservation) throws ReservationNotPossibleException {
        BSRequest bsRequest = new BSRequest(RequestType.MakeReservation, gson.toJson(reservation));
        Response response = handleRequest(bsRequest);
        if(response.getType() == ResponseType.ERROR)
            throw new ReservationNotPossibleException(response.getMessage());
    }

    @Override
    public void removeReservation(Reservation reservation) {
        BSRequest bsRequest = new BSRequest(RequestType.RemoveReservation, gson.toJson(reservation));
        Response response = handleRequest(bsRequest);
        if(response.getType() == ResponseType.ERROR){
            System.out.println("------------------- CEVA ERAORE----------------------");
        }
    }

    @Override
    public abstract void run();


    public void updateChanges(Response response){
        Reservation res = gson.fromJson(response.getjSonResponse(),Reservation.class);
        if(response.getType() == ResponseType.ReservationAdded){
            mainClientView.reservationsList.add(res);
            for(Excursion ex: mainClientView.excursionsList){
                System.out.println("ids: " + ex.getExcursionId() + "  " + res.getExcursion().getExcursionId());
                if(ex.getExcursionId() == res.getExcursion().getExcursionId()){
                    ex.setAvailableSeats(ex.getAvailableSeats() - res.getTickets());
                }
            }
            mainClientView.excursionTable.refresh();
        } else if (response.getType() == ResponseType.ReservationDeleted) {
            int index = 0;
            for(Reservation crtRes : mainClientView.reservationsList){
                if (crtRes.getResId() == res.getResId()){
                    break;
                }
                index++;
            }
            mainClientView.reservationsList.remove(index);

            for(Excursion ex: mainClientView.excursionsList){
                if(ex.getExcursionId() == res.getExcursion().getExcursionId()){
                    ex.setAvailableSeats(ex.getAvailableSeats() + res.getTickets());
                }
            }
            mainClientView.reservationTable.refresh();
            mainClientView.excursionTable.refresh();
        }
    }
}
