import com.google.gson.Gson;
import com.sun.org.apache.regexp.internal.RE;
import exceptions.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Excursion;
import model.Reservation;
import model.Users;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ClientService extends AbstractClientService{
    private final static String host = "localHost";
    private final static int port = 1234;

    private Socket socket = null;
    private Socket notificationSocket = null;

    private ObjectOutputStream outputStream = null;
    private boolean isConnected = false;

    public void setMainClientView(MainClientView mainClientView){
        this.mainClientView = mainClientView;
    }


    public ClientService() throws CouldNotConnectToSocketException {
        gson = new Gson();
        this.mainClientView = null;
        if(openSocket() == false)
            throw new CouldNotConnectToSocketException("Could not connect to server");
    }

    private boolean openSocket(){
        try{
            socket = new Socket(host, port);
            notificationSocket = new Socket(host,port);

            return true;
        }catch (SocketException ex){
            ex.printStackTrace();
            System.out.println("Could not connect to socket");
            return false;
        }catch (IOException ex2){
            ex2.printStackTrace();
            System.out.println("IOException while connecting to socket");
            return false;
        }
    }

    public void run(){
        while (notificationSocket.isConnected()) {
            try {
                ObjectOutputStream out = new ObjectOutputStream(notificationSocket.getOutputStream());
                out.writeObject("Waiting");


                ObjectInputStream in = new ObjectInputStream(notificationSocket.getInputStream());

                Response response = (Response) in.readObject();
                System.out.println(response);

                updateChanges(response);

            } catch (IOException ioex) {
                System.out.println("Closed connection with notification server");
                System.out.println(ioex);
                return;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized Response handleRequest(BSRequest bsRequest) {
        if (socket.isConnected()){
            isConnected = true;
            try {
                Response response = null;
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(bsRequest);
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                response = (Response) inputStream.readObject();
                return response;
            } catch (IOException ioex) {
                System.out.println(ioex);
            } catch (ClassNotFoundException nf) {
                nf.printStackTrace();
            }
        }else{
            isConnected = false;
            System.out.println("Socket not connected");
        }
        return null;
    }

    public void closeConnection() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isConnected = false;
    }
}
