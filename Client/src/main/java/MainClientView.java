import exceptions.EntityNotFoundException;
import exceptions.ReservationNotPossibleException;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import model.Excursion;
import model.Reservation;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class MainClientView {
    Reservation currentReservation;
    private List<Excursion> excursii = new ArrayList<>();
    BorderPane borderPane;

    TableView<Excursion> excursionTable;
    TableView<Reservation> reservationTable;

    @SuppressWarnings("rawtypes")
    TableColumn excursionTableExcursionId;
    @SuppressWarnings("rawtypes")
    TableColumn excursionTableAttrId;
    @SuppressWarnings("rawtypes")
    TableColumn excursionTableFirmId;
    @SuppressWarnings("rawtypes")
    TableColumn excursionTableDepartTime;
    @SuppressWarnings("rawtypes")
    TableColumn excursionTablePrice;
    @SuppressWarnings("rawtypes")
    TableColumn excursionTableAvailableSeats;

    @SuppressWarnings("rawtypes")
    TableColumn reservationTableResId;
    @SuppressWarnings("rawtypes")
    TableColumn reservationTableExcursionId;
    @SuppressWarnings("rawtypes")
    TableColumn reservationTableName;
    @SuppressWarnings("rawtypes")
    TableColumn reservationTablePhone;
    @SuppressWarnings("rawtypes")
    TableColumn reservationTableTickets;

    TextField excursionIdField;

    TextField nameField;
    TextField phoneField;
    TextField ticketsField;



    Button buttonAnulare;
    Button buttonVanzare;


    public ObservableList<Excursion> excursionsList;
    public ObservableList<Reservation> reservationsList;

    private AbstractClientService clientService;

    private TextField numeAtrField;
    private TextField startTimeField;
    private TextField endTimeField;


    @SuppressWarnings("rawtypes")
    public MainClientView(AbstractClientService clientService) {

        currentReservation = null;

        this.clientService = clientService;
        this.clientService.setMainClientView(this);

        new Thread(clientService).start();

        this.excursionsList = clientService.getAllExcursions();
        this.reservationsList = clientService.getAllReservations();

        this.excursionTable = new TableView<>();
        this.excursionTable.setPrefWidth(450);
        this.reservationTable = new TableView<>();
        this.reservationTable.setPrefWidth(450);

        this.excursionTableExcursionId = new TableColumn("ExcursionId");
        this.excursionTableAttrId = new TableColumn("Attraction");
        this.excursionTableFirmId = new TableColumn("Firm");
        this.excursionTableDepartTime = new TableColumn("DepartTime");
        this.excursionTablePrice = new TableColumn("Price");
        this.excursionTableAvailableSeats = new TableColumn("AvailableSeats");



        this.excursionTableExcursionId.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.1));
        this.excursionTableAttrId.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.15));
        this.excursionTableFirmId.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.15));
        this.excursionTableDepartTime.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.2));
        this.excursionTablePrice.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.2));
        this.excursionTableAvailableSeats.prefWidthProperty().bind(excursionTable.widthProperty().multiply(0.2));

        excursionTableExcursionId.setResizable(false);
        excursionTableAttrId.setResizable(false);
        excursionTableFirmId.setResizable(false);
        excursionTableDepartTime.setResizable(false);
        excursionTablePrice.setResizable(false);
        excursionTableAvailableSeats.setResizable(false);

        // --------------------
        this.reservationTableResId = new TableColumn("ResId");
        this.reservationTableExcursionId = new TableColumn("ExcursionId");
        this.reservationTableName = new TableColumn("Name");
        this.reservationTablePhone = new TableColumn("Phone");
        this.reservationTableTickets = new TableColumn("Tickets");

        this.reservationTableResId.prefWidthProperty().bind(reservationTable.widthProperty().multiply(0.1));
        this.reservationTableExcursionId.prefWidthProperty().bind(reservationTable.widthProperty().multiply(0.1));
        this.reservationTableName.prefWidthProperty().bind(reservationTable.widthProperty().multiply(0.3));
        this.reservationTablePhone.prefWidthProperty().bind(reservationTable.widthProperty().multiply(0.3));
        this.reservationTableTickets.prefWidthProperty().bind(reservationTable.widthProperty().multiply(0.3));

        reservationTableResId.setResizable(false);
        reservationTableExcursionId.setResizable(false);
        reservationTableName.setResizable(false);
        reservationTableTickets.setResizable(false);





        this.excursionIdField = new TextField();
        this.excursionIdField.setPromptText("ExcursionId");
        //--
        this.nameField = new TextField();
        this.nameField.setPromptText("Nume");
        this.phoneField = new TextField();
        this.phoneField.setPromptText("Phone");
        this.ticketsField = new TextField();
        this.ticketsField.setPromptText("Tickets");

        this.numeAtrField = new TextField();
        this.numeAtrField.setPromptText("Atractie");

        this.startTimeField = new TextField();
        this.startTimeField.setPromptText("Start Time");

        this.endTimeField = new TextField();
        this.endTimeField.setPromptText("End Time");




        this.buttonAnulare = new Button("Anuleaza");
        this.buttonVanzare = new Button("Vanzare");
        this.initBorderPane();
    }

    public BorderPane getView() {
        return this.borderPane;
    }

    private void initBorderPane() {
        borderPane = new BorderPane();
        borderPane.setCenter(initCenter());
    }


    private Node initCenter() {
        AnchorPane anchorPane = new AnchorPane();

        HBox denumireBox = new HBox(10, new Label("Cod Excursie: "), this.excursionIdField);
        denumireBox.setAlignment(Pos.CENTER_RIGHT);

        HBox numePBox = new HBox(10, new Label("Nume: "), this.nameField);
        numePBox.setAlignment(Pos.CENTER_RIGHT);

        HBox phoneBox = new HBox(10, new Label("Phone"), this.phoneField);
        phoneBox.setAlignment(Pos.CENTER_RIGHT);

        HBox ticksBox = new HBox(10, new Label("Tickets: "), this.ticketsField);
        ticksBox.setAlignment(Pos.CENTER_RIGHT);

        HBox butoane = new HBox(10,buttonAnulare,buttonVanzare);


        VBox textField = new VBox(10, denumireBox, numePBox, ticksBox, phoneBox, butoane);


        Label t1 = new Label("Excursii");
        t1.setFont(new Font("Calibri",40));
        Label t2 =  new Label("Rezervari");
        t2.setFont(new Font("Calibri",40));



        HBox titles = new HBox(400, t1 , t2);
        titles.setPadding(new Insets(10,0,0,100));
        titles.setAlignment(Pos.TOP_CENTER);



        HBox numeFilterBox = new HBox(10, new Label("Nume Atr:"), this.numeAtrField);
        numeFilterBox.setAlignment(Pos.BOTTOM_LEFT);
        HBox startBox = new HBox(10, new Label("Start:"), this.startTimeField);
        startBox.setAlignment(Pos.BOTTOM_LEFT);
        HBox endBox = new HBox(10, new Label("End"), this.endTimeField);
        endBox.setAlignment(Pos.BOTTOM_LEFT);
        HBox filteres = new HBox(30,numeFilterBox,startBox,endBox);


        anchorPane.getChildren().add(filteres);
        anchorPane.getChildren().add(titles);
        anchorPane.getChildren().add(this.excursionTable);
        anchorPane.getChildren().add(this.reservationTable);
        anchorPane.getChildren().add(textField);


        this.initTableExcursion();
        this.initTableReservations();

        AnchorPane.setTopAnchor(this.excursionTable, 80d);
        AnchorPane.setLeftAnchor(this.excursionTable, 30d);


        AnchorPane.setTopAnchor(this.reservationTable, 80d);
        AnchorPane.setLeftAnchor(this.reservationTable, 500d);

        AnchorPane.setTopAnchor(textField, 180d);
        AnchorPane.setRightAnchor(textField, 30d);

        AnchorPane.setBottomAnchor(filteres,30d);
        AnchorPane.setLeftAnchor(filteres,30d);


        this.handleFiltrare(excursionsList);


        this.buttonVanzare.setOnAction(event ->{
            if(checkFieldText()) {
                try {
                    Excursion ex = clientService.getExcursion(Integer.parseInt(excursionIdField.getText()));
                    Reservation res = new Reservation(ex, nameField.getText(), phoneField.getText(), Integer.parseInt(ticketsField.getText()));
                    clientService.makeReservation(res);
                } catch (EntityNotFoundException ex) {
                    warning(ex.getMessage());
                } catch (ReservationNotPossibleException e) {
                    warning(e.getMessage());
                }
            }
        });


        this.buttonAnulare.setOnAction(event ->{
            try{
                if(currentReservation!=null)
                    clientService.removeReservation(currentReservation);
                else
                    warning("Selectati o rezervare");
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });


        this.excursionTable.setRowFactory(tr -> {
            TableRow<Excursion> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                Excursion f = row.getItem();
                if (f != null) {
                    this.excursionIdField.setText(Integer.toString(f.getExcursionId()));
                    this.nameField.setText("");
                    this.phoneField.setText("");
                    this.ticketsField.setText("");
                }
            });
            return row;
        });


        this.reservationTable.setRowFactory(tr -> {
            TableRow<Reservation> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                Reservation f = row.getItem();
                if (f != null) {
                    this.excursionIdField.setText(Integer.toString(f.getExcursion().getExcursionId()));
                    this.nameField.setText(f.getClientName());
                    this.phoneField.setText(f.getPhone());
                    this.ticketsField.setText(Integer.toString(f.getTickets()));
                    this.currentReservation = f;
                }
            });
            return row;
        });

        return anchorPane;
    }

    private void warning(String war) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setContentText(war);
        alert.showAndWait();
    }

    private void error(String err) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(err);
        alert.showAndWait();
    }

    private boolean checkFieldText() {
        if (this.nameField.getText().isEmpty() || this.phoneField.getText().isEmpty() || this.ticketsField.getText().isEmpty() ) {
            this.warning("Exista campuri vide la vanzare !");
            return false;
        }
        return true;
    }


    private void clearTextField() {
        this.nameField.clear();
        this.phoneField.clear();
        this.ticketsField.clear();
    }

    @SuppressWarnings("unchecked")
    private void initTableExcursion() {

        // Adding cellTable
        this.excursionTable.getColumns().addAll(this.excursionTableExcursionId, this.excursionTableAttrId, this.excursionTableFirmId, this.excursionTableDepartTime, this.excursionTablePrice, this.excursionTableAvailableSeats);

        // Set value cellTable
        this.excursionTableExcursionId.setCellValueFactory(new PropertyValueFactory<Excursion, Integer>("ExcursionId"));
        this.excursionTableAttrId.setCellValueFactory(new PropertyValueFactory<Excursion, String>("attraction"));
        this.excursionTableFirmId.setCellValueFactory(new PropertyValueFactory<Excursion, String>("firm"));
        this.excursionTableDepartTime.setCellValueFactory(new PropertyValueFactory<Excursion, String>("DepartTime"));
        this.excursionTablePrice.setCellValueFactory(new PropertyValueFactory<Excursion, String>("Price"));
        this.excursionTableAvailableSeats.setCellValueFactory(new PropertyValueFactory<Excursion, String>("AvailableSeats"));


        this.excursionTable.setItems(this.excursionsList);
        this.excursionTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }


    @SuppressWarnings("unchecked")
    private void initTableReservations() {


        // Adding cellTable
        this.reservationTable.getColumns().addAll(this.reservationTableResId,this.reservationTableExcursionId, this.reservationTableName, this.reservationTablePhone, this.reservationTableTickets);

        // Set value cellTable
        this.reservationTableResId.setCellValueFactory(new PropertyValueFactory<Reservation, String>("ResId"));
        this.reservationTableExcursionId.setCellValueFactory(new PropertyValueFactory<Reservation, String>("excursion"));
        this.reservationTableName.setCellValueFactory(new PropertyValueFactory<Reservation, String>("ClientName"));
        this.reservationTablePhone.setCellValueFactory(new PropertyValueFactory<Reservation, String>("Phone"));
        this.reservationTableTickets.setCellValueFactory(new PropertyValueFactory<Reservation, String>("Tickets"));

        this.reservationTable.setItems(this.reservationsList);
        this.reservationTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    public void reloadTable(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                excursionsList = clientService.getAllExcursions();
                excursionTable.setItems(excursionsList);

                reservationsList = clientService.getAllReservations();
                reservationTable.setItems(reservationsList);

                handleFiltrare(excursionsList);
            }
        });
    }




    private void handleFiltrare(ObservableList<Excursion> list){

        FilteredList<Excursion> filteredList = new FilteredList<Excursion>(list, p -> true);
        filteredList.predicateProperty().bind(Bindings.createObjectBinding(() ->
                        excursion -> excursion.getAttraction().getName().contains(this.numeAtrField.getText()) &&
                                excursion.getDepartTime().after(getSTime()) && excursion.getDepartTime().before(getETime()),
                this.numeAtrField.textProperty(),
                this.startTimeField.textProperty(),
                this.endTimeField.textProperty())
        );

        SortedList<Excursion> sortedData = new SortedList<>(filteredList);
        sortedData.comparatorProperty().bind(this.excursionTable.comparatorProperty());
        this.excursionTable.setItems(sortedData);
    }

    public Time getSTime(){
        Time sTimeTime = new Time(0,0,1);
        if(!startTimeField.getText().isEmpty() && startTimeField.getText().matches("[0-9][0-9]?:[0-9][0-9]?:[0-9][0-9]?")){
            String sTime = startTimeField.getText();
            String[] oreS = sTime.split(":");
            Integer oreSInt[] = {0,0,0};
            for(int i=0;i<oreS.length;i++){
                oreSInt[i] = Integer.parseInt(oreS[i]);
            }

            sTimeTime = new Time(oreSInt[0],oreSInt[1],oreSInt[2]);
        }

        Time sTimeFinal = new Time(sTimeTime.getTime());

        return sTimeFinal;
    }

    public Time  getETime(){
        Time eTimeTime = new Time(23,59,59);

        if(!endTimeField.getText().isEmpty() && endTimeField.getText().matches("[0-9][0-9]?:[0-9][0-9]?:[0-9][0-9]?")){

            String eTime = endTimeField.getText();
            String[] oreE = eTime.split(":");
            Integer oreEInt[] = {0,0,0};
            for(int i=0;i<oreE.length;i++){
                oreEInt[i] = Integer.parseInt(oreE[i]);
                eTimeTime = new Time(oreEInt[0],oreEInt[1],oreEInt[2]);
            }
        }

        Time eTimeFinal = new Time(eTimeTime.getTime());
        return eTimeFinal;
    }

}
