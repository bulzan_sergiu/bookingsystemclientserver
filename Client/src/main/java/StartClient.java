import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by sergiubulzan on 09/04/2017.
 */
public class StartClient extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        Stage myStage = new Stage();

        myStage.setTitle("SignIn");

        AbstractClientService clientService = new RabMQClient();

        LogInView ofView = new LogInView(myStage, clientService);
        Parent p = ofView.getView();


        myStage.setScene(new Scene(p,1300,600));
        myStage.show();
    }
}
