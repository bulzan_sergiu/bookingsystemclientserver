import exceptions.EntityNotFoundException;
import exceptions.ExistingUserException;
import exceptions.ReservationNotPossibleException;
import exceptions.SignInException;
import javafx.collections.ObservableList;
import model.Excursion;
import model.Reservation;
import model.Users;

/**
 * Created by sergiubulzan on 13/05/2017.
 */
public interface IClientService extends Runnable {

    void setMainClientView(MainClientView mainClientView);

    boolean sendSignUpRequest(Users user) throws ExistingUserException;

    boolean sendSignInRequest(Users user) throws SignInException;

    ObservableList<Excursion> getAllExcursions();

    ObservableList<Reservation> getAllReservations();

    Excursion getExcursion(int id) throws EntityNotFoundException ;

    void makeReservation(Reservation reservation) throws ReservationNotPossibleException;

    void removeReservation(Reservation reservation);

    Response handleRequest(BSRequest bsRequest);
}
