Application created for the "Medii de proiectare si programare" laboratory. 

In involves a client-server application, both the client and the server being written in java. 

![Screen Shot 2017-05-16 at 20.35.45.png](https://bitbucket.org/repo/EgnK8KX/images/2712238912-Screen%20Shot%202017-05-16%20at%2020.35.45.png)

What I have learned: 

* Connection to a MySQL database 
* TCP Communication using sockets (also implemented notification system) 
* Client(java) - Server(c#) communication using JSONs
* Java RMI
* Mapping entity classes with the database using Hibernate Framework
* Improved notification system and communication using Message Queues - RabbitmQ