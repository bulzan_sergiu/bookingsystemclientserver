package util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by sergiubulzan on 07/05/2017.
 */
public class HibernateUtil {

    private  SessionFactory sessionFactory;
    private  ServiceRegistry serviceRegistry;

    private HibernateUtil(){
        initialize();
    }

    public static HibernateUtil instance = new HibernateUtil();

    void initialize() {
        // A SessionFactory is set up once for an application!

        Configuration configuration = new Configuration();
        configuration.configure();
        configuration.addResource("/reservation.hbm.xml");

        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    }

    void close() {
        if ( sessionFactory != null ) {
            sessionFactory.close();
        }
    }

    public SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }
}
