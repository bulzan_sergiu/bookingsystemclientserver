package repository;

import model.Firm;
import util.JdbcUtils;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class FirmRepo implements IRepository<Integer, Firm> {
    private JdbcUtils dbUtils;

    public FirmRepo(Properties props){
        dbUtils=new JdbcUtils(props);
    }
    public FirmRepo(JdbcUtils utils){
        dbUtils=utils;
    }


    @Override
    public int size() {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("select count(*) as [SIZE] from Firms")) {
            try(ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        }catch(SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return 0;
    }

    @Override
    public void save(Firm entity) {
        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("insert into Firms values (?,?)")) {
            preStmt.setInt(1, entity.getFirmId());
            preStmt.setString(2, entity.getName());

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void delete(Integer integer) {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("delete from Firms where FirmId=?")){
            preStmt.setInt(1,integer);
            int result=preStmt.executeUpdate();
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }

    @Override
    public void update(Integer integer, Firm entity) {
        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("UPDATE Firms SET " +
                "FirmId=? , Name=? " +
                "WHERE FirmId = ?" )){
            preStmt.setInt(1,entity.getFirmId());
            preStmt.setString(2, entity.getName());
            preStmt.setInt(3, integer);

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public Firm findOne(Integer integer) {
        Connection con=dbUtils.getConnection();

        try(PreparedStatement preStmt=con.prepareStatement("select * from Firms where FirmId=?")){
            preStmt.setInt(1,integer);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("FirmId");
                    String name = result.getString("Name");

                    Firm firm = new Firm(id,name);

                    return firm;
                }
            }
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return null;    }

    @Override
    public List<Firm> findAll() {
        Connection con=dbUtils.getConnection();
        List<Firm> firms =new ArrayList<>();
        try(PreparedStatement preStmt=con.prepareStatement("select * from Firms")) {
            try(ResultSet result=preStmt.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("FirmId");
                    String name = result.getString("Name");

                    Firm firm = new Firm(id,name);


                    firms.add(firm);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return firms;
    }
}
