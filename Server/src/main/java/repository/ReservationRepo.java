package repository;

import model.Excursion;
import model.Reservation;
import util.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ReservationRepo implements IRepository<Integer, Reservation> {
    private JdbcUtils dbUtils;

    public ReservationRepo(Properties props){
        dbUtils=new JdbcUtils(props);
    }
    public ReservationRepo(JdbcUtils utils){
        dbUtils=utils;
    }

    @Override
    public int size() {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("select count(*) as [SIZE] from Reservation")) {
            try(ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        }catch(SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return 0;
    }

    @Override
    public void delete(Integer integer) {
        Connection con = dbUtils.getConnection();

        try (CallableStatement calStmt = con.prepareCall("{ call removeReservation(?) }")) {

            calStmt.setInt(1, integer);

            boolean result = calStmt.execute();
            if(result == true){
                System.out.println("removed all good");
            }else{
                System.out.println("removed all bad");

            }
        } catch (SQLException ex) {
            System.out.println("DB error: " + ex);
        }
    }

    @Override
    public void update(Integer integer, Reservation entity) {
        System.out.println("Cannot update reservation for now");
    }

    @Override
    public Reservation findOne(Integer integer) {
        System.out.println("Cannot find reservation for now");
        return null;
    }

    @Override
    public List<Reservation> findAll() {
        Connection con = dbUtils.getConnection();
        List<Reservation> reservations = new ArrayList<>();
        try (PreparedStatement preStmt = con.prepareStatement("select * from Reservation")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    int resId = result.getInt("resId");
                    int exI = result.getInt("ExcursionId");
                    String name = result.getString("Name");
                    String phone = result.getString("Phone");
                    int tickets = result.getInt("Tickets");

                    ExcursionRepo excursionRepo = new ExcursionRepo(dbUtils);
                    Excursion exc = excursionRepo.findOne(exI);
                    Reservation resv = new Reservation(resId, exc, name, phone, tickets);


                    reservations.add(resv);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return reservations;
    }


    public void save(Reservation entity){
        Connection con = dbUtils.getConnection();

        try (CallableStatement calStmt = con.prepareCall("{ call reserve(?,?,?,?) }")) {

            calStmt.setInt(1, entity.getExcursion().getExcursionId());
            calStmt.setString(2,entity.getClientName());
            calStmt.setString(3,entity.getPhone());
            calStmt.setInt(4,entity.getTickets());

            calStmt.execute();
        } catch (SQLException ex) {
            System.out.println("Nu mai exista locuri disponibile!  " );
        }
    }
}
