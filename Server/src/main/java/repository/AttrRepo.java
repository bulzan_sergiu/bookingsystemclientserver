package repository;

import model.Attraction;
import util.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class AttrRepo implements IRepository<Integer, Attraction> {

    private JdbcUtils dbUtils;

    public AttrRepo(Properties props){
        dbUtils=new JdbcUtils(props);
    }
    public AttrRepo(JdbcUtils utils){
        dbUtils=utils;
    }


    @Override
    public int size() {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("select count(*) as [SIZE] from Attractions")) {
            try(ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        }catch(SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return 0;
    }

    @Override
    public void save(Attraction entity) {
        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("insert into Attractions values (?,?)")) {
            preStmt.setInt(1, entity.getAttrId());
            preStmt.setString(2, entity.getName());

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void delete(Integer integer) {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("delete from Attractions where AttrId=?")){
            preStmt.setInt(1,integer);
            int result=preStmt.executeUpdate();
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }

    @Override
    public void update(Integer integer, Attraction entity) {
        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("UPDATE Attractions SET " +
                "AttrId=? , Name=? " +
                "WHERE AttrId = ?" )){
            preStmt.setInt(1,entity.getAttrId());
            preStmt.setString(2, entity.getName());
            preStmt.setInt(3, integer);

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public Attraction findOne(Integer integer) {
        Connection con=dbUtils.getConnection();

        try(PreparedStatement preStmt=con.prepareStatement("select * from Attractions where AttrId=?")){
            preStmt.setInt(1,integer);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("AttrId");
                    String name = result.getString("Name");

                    Attraction attr = new Attraction(id,name);

                    return attr;
                }
            }
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return null;
    }

    @Override
    public List<Attraction> findAll() {
        Connection con=dbUtils.getConnection();
        List<Attraction> attractions =new ArrayList<>();
        try(PreparedStatement preStmt=con.prepareStatement("select * from Attractions")) {
            try(ResultSet result=preStmt.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("AttrId");
                    String name = result.getString("Name");

                    Attraction attr = new Attraction(id,name);


                    attractions.add(attr);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return attractions;    }
}
