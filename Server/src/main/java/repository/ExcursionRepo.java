package repository;

import model.Attraction;
import model.Excursion;
import model.Firm;
import util.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ExcursionRepo implements IRepository<Integer, Excursion> {
    private JdbcUtils dbUtils;

    public ExcursionRepo(Properties props){
        dbUtils=new JdbcUtils(props);
    }

    public ExcursionRepo(JdbcUtils dbutils){
        this.dbUtils=dbutils;
    }

    public int size() {
        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("select count(*) as [SIZE] from Excursion")) {
            try(ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        }catch(SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return 0;
    }

    public void save(Excursion entity) {


        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("insert into Excursion values (?,?,?,?,?,?)")) {
            preStmt.setInt(1, entity.getExcursionId());
            preStmt.setInt(2, entity.getAttraction().getAttrId());
            preStmt.setInt(3, entity.getFirm().getFirmId());
            preStmt.setTime(4, entity.getDepartTime());
            preStmt.setInt(5, entity.getPrice());
            preStmt.setInt(6, entity.getAvailableSeats());


            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }

    }


    public void delete(Integer integer) {

        Connection con=dbUtils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("delete from Excursion where id=?")){
            preStmt.setInt(1,integer);
            int result=preStmt.executeUpdate();
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }


    public Excursion findOne(Integer integer) {

        Connection con=dbUtils.getConnection();

        try(PreparedStatement preStmt=con.prepareStatement("select * from Excursion where ExcursionID=?")){
            preStmt.setInt(1,integer);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("ExcursionID");
                    int attrId = result.getInt("AttrId");
                    int firmId = result.getInt("firmId");
                    Time departTime = result.getTime("DepartTime");
                    int price = result.getInt("price");
                    int aSeats = result.getInt("AvailableSeats");

                    AttrRepo attrRepo = new AttrRepo(dbUtils);
                    FirmRepo firmRepo = new FirmRepo(dbUtils);

                    Attraction atr = attrRepo.findOne(attrId);
                    Firm firm = firmRepo.findOne(firmId);


                    Excursion excursion = new Excursion(id,atr,firm,departTime,price,aSeats);

                    return excursion;
                }
            }
        }catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return null;
    }


    public void update(Integer integer, Excursion entity) {

        Connection con = dbUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("UPDATE Excursion SET " +
                "ExcursionId=?, AttrId=?,FirmId=?,DepartTime=?,Price=?,AvailableSeats=? " +
                "WHERE ExcursionId = ?" )){
            preStmt.setInt(1, entity.getExcursionId());
            preStmt.setInt(2, entity.getAttraction().getAttrId());
            preStmt.setInt(3, entity.getFirm().getFirmId());
            preStmt.setTime(4, entity.getDepartTime());
            preStmt.setInt(5, entity.getPrice());
            preStmt.setInt(6, entity.getAvailableSeats());
            preStmt.setInt(7, integer);

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    public List<Excursion> findAll() {

        Connection con=dbUtils.getConnection();
        List<Excursion> excurions =new ArrayList<>();
        try(PreparedStatement preStmt=con.prepareStatement("select * from Excursion")) {
            try(ResultSet result=preStmt.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("ExcursionID");
                    int attrId = result.getInt("AttrId");
                    int firmId = result.getInt("firmId");
                    Time departTime = result.getTime("DepartTime");
                    int price = result.getInt("price");
                    int aSeats = result.getInt("AvailableSeats");

                    AttrRepo attrRepo = new AttrRepo(dbUtils);
                    FirmRepo firmRepo = new FirmRepo(dbUtils);

                    Attraction atr = attrRepo.findOne(attrId);
                    Firm firm = firmRepo.findOne(firmId);


                    Excursion excursion = new Excursion(id,atr,firm,departTime,price,aSeats);


                    excurions.add(excursion);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return excurions;
    }
}
