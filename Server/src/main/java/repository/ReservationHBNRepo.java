package repository;

import com.sun.org.apache.regexp.internal.RE;
import model.Excursion;
import model.Reservation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import util.HibernateUtil;
import util.JdbcUtils;

import java.util.List;

/**
 * Created by sergiubulzan on 07/05/2017.
 */
public class ReservationHBNRepo implements IRepository<Integer, Reservation> {

    @Override
    public int size() {
        SessionFactory sessionFactory = HibernateUtil.instance.getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Integer count = (Integer) ((Long) session.createQuery("select count(*) from Reservation").uniqueResult()).intValue();

        session.getTransaction().commit();
        session.close();

        return count;

    }

    @Override
    public void save(Reservation entity) {
        SessionFactory sessionFactory = HibernateUtil.instance.getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Query query = session.getNamedQuery("reserve").setParameter("excursionId", entity.getExcursionId())
                .setParameter("clientName", entity.getClientName())
                .setParameter("phone",entity.getPhone())
                .setParameter("tickets",entity.getTickets());

        query.executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Integer integer) {
        SessionFactory sessionFactory = HibernateUtil.instance.getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Query query = session.getNamedQuery("removeReservation")
                .setParameter("reservationId", integer);

        query.executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Integer integer, Reservation entity) {

    }

    @Override
    public Reservation findOne(Integer integer) {
        return null;
    }

    @Override
    public List<Reservation> findAll() {
        SessionFactory sessionFactory = HibernateUtil.instance.getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List result =  session.createQuery( "from Reservation" ).list();

        ExcursionRepo excursionRepo = new ExcursionRepo(JdbcUtils.getProps());
        for(Reservation res : (List<Reservation>) result){
            Excursion exc = excursionRepo.findOne(res.getExcursionId());
            res.setExcursion(exc);
        }

        session.getTransaction().commit();
        session.close();

        return  (List<Reservation>) result;
    }
}
