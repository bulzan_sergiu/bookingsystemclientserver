import com.google.gson.Gson;
import exceptions.ExistingUserException;
import model.Excursion;
import model.Reservation;
import model.Users;
import services.ExcursionService;
import services.ReservationService;
import services.UsersService;

import java.net.Socket;
import java.util.List;

/**
 * Created by sergiubulzan on 14/05/2017.
 */
public abstract class AbstractRequestHandler implements Runnable {

    Gson gson;

    UsersService usersService;
    ReservationService reservationService;
    ExcursionService excursionService;

    public AbstractRequestHandler(UsersService usersService, ReservationService reservationService, ExcursionService excursionService) {
        this.usersService = usersService;
        this.reservationService = reservationService;
        this.excursionService = excursionService;
        gson = new Gson();
    }

    @Override
    public abstract void run();


    protected Response handleSignUp(String usrJsonString){
        Users user = gson.fromJson(usrJsonString, Users.class);
        try{
            usersService.addUser(user);

            return new Response(ResponseType.OK,"Am adaugat userul");
        }catch (ExistingUserException ex){
            return new Response(ResponseType.ERROR, "User existent");
        }
    }

    protected Response handleSignIn(String usrJsonString){
        Users user = gson.fromJson(usrJsonString, Users.class);
        Users found = usersService.findByUsername(user.getUsername());
        if(found != null){
            if(found.getPassword().equals(user.getPassword())){
                return new Response(ResponseType.OK, "SignIn cu success");
            }else{
                return new Response(ResponseType.ERROR,"Imi pare rau, parola gresita!" );
            }
        }else{
            return new Response(ResponseType.ERROR, "Imi pare rau, username invalid!" );
        }
    }

    protected Response handleGetAllExcursions() {
        List<Excursion> excursions = excursionService.getAll();
        Excursion[] excursionsArray = new Excursion[excursions.size()];
        int i=0;
        for(Excursion exc : excursions){
            excursionsArray[i++] = exc;
        }
        String jsonExcursionsString = gson.toJson(excursionsArray);
        System.out.println(jsonExcursionsString);
        return new Response(ResponseType.OK,"AllExcursionsReturnes", jsonExcursionsString);
    }

    protected Response handleGetAllReservations() {
        List<Reservation> reservations = reservationService.getAll();
        Reservation[] reservationsArray = new Reservation[reservations.size()];

        int i=0;
        for(Reservation res : reservations){
            reservationsArray[i++] = res;
        }

        return new Response(ResponseType.OK,"AllExcursionsReturnes", gson.toJson(reservationsArray));
    }

    protected Response handleGetExcursion(String id){
        Excursion ex = excursionService.findExcursion(Integer.parseInt(id));
        if(ex == null)
            return new Response(ResponseType.ERROR, "Could not find excursion with this id");
        return new Response(ResponseType.OK, "Excursion Sent", gson.toJson(ex));
    }

    protected Response handleMakeReservation(String jSonReservation){
        Reservation reservation = gson.fromJson(jSonReservation,Reservation.class);
        this.reservationService.reserve(reservation);
        Response resp = new Response(ResponseType.ReservationAdded, "Reservation made");
        resp.jSonResponse = jSonReservation;
        sendNotification(resp);
        return new Response(ResponseType.OK,"Reservation successful");
    }

    protected Response handleRemoveReservation(String jSonReservation){
        Reservation reservation = gson.fromJson(jSonReservation,Reservation.class);
        this.reservationService.deleteReservation(reservation.getResId());
        Response resp = new Response(ResponseType.ReservationDeleted, "Reservation deleted");
        resp.jSonResponse = jSonReservation;
        sendNotification(resp);
        return new Response(ResponseType.OK,"Reservation deleted sucessfuly");
    }

    public Response getResponseForRequest(BSRequest bsRequest){
        if(bsRequest != null){

            Response response;
            switch (bsRequest.requestType) {
                case SignUp:
                    response = handleSignUp(bsRequest.getJsonString());
                    return response;
                case SignIn:
                    response = handleSignIn(bsRequest.getJsonString());
                    return response;
                case GetAllExcursions:
                    response = handleGetAllExcursions();
                    return response;
                case GetAllReservations:
                    response = handleGetAllReservations();
                    return response;
                case GetExcursion:
                    response = handleGetExcursion(bsRequest.getJsonString());
                    return response;
                case MakeReservation:
                    response = handleMakeReservation(bsRequest.getJsonString());
                    return response;
                case RemoveReservation:
                    response = handleRemoveReservation(bsRequest.getJsonString());
                    return response;
            }
        }
        return null;
    }


    protected abstract void sendNotification(Response response);


}
