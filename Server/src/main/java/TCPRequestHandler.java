import com.google.gson.Gson;
import services.ExcursionService;
import services.ReservationService;
import services.UsersService;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class TCPRequestHandler extends AbstractRequestHandler {
    private ObjectInputStream inStream = null;
    ObjectOutputStream outputStream = null;

    Response shouldUpdateResponse = null;


    private Socket client;
    public boolean isConnected = false;

    TCPServer server;


    Gson gson;

    public TCPRequestHandler(Socket client, UsersService usersService, ReservationService reservationService, ExcursionService excursionService, TCPServer server) {
        super(usersService,reservationService,excursionService);
        this.client = client;
        this.server = server;
        gson = new Gson();
    }

    public boolean isConnected(){
        return isConnected;
    }

    @Override
    public void run() {
        try {
            isConnected = true;
            while (client.isConnected()){
                inStream = new ObjectInputStream(client.getInputStream());
                outputStream = new ObjectOutputStream(client.getOutputStream());

                BSRequest bsRequest = null;

                try{
                    bsRequest = (BSRequest) inStream.readObject();
                }catch (IOException ex22){
                    ;
                }

                Response response = getResponseForRequest(bsRequest);
                outputStream.writeObject(response);
            }
        } catch (SocketException se) {
            se.printStackTrace();
        } catch (IOException e) {
            System.out.println("Client on thread " + Thread.currentThread() + " disconnected");
            //e.printStackTrace();
        } catch (ClassNotFoundException cn) {
            cn.printStackTrace();
        }
        System.out.println("Thread closed");
        isConnected = false;
    }

    @Override
    protected void sendNotification(Response response) {
        server.shouldUpdate(response);
    }

}