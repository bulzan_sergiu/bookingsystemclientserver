import com.rabbitmq.client.*;
import services.ExcursionService;
import services.ReservationService;
import services.UsersService;

import java.io.IOException;


/**
 * Created by sergiubulzan on 14/05/2017.
 */
public class RabMQRequestHandler extends AbstractRequestHandler {

    Channel channel;
    Connection connection;

    public RabMQRequestHandler(UsersService usersService, ReservationService reservationService, ExcursionService excursionService, Channel channel, Connection connection) {
        super(usersService,reservationService,excursionService);
        this.channel = channel;
        this.connection = connection;
    }

    @Override
    public void run() {
        try {
            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();

                    Response response = null;

                    try {
                        BSRequest bsRequest = (BSRequest) Serialization.deserialize(body);

                        response = getResponseForRequest(bsRequest);

                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e.toString());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } finally {

                        byte[] responseData = Serialization.serialize(response);

                        System.out.println("Reply to: " + properties.getReplyTo());
                        channel.basicPublish("", properties.getReplyTo(), replyProps, responseData);
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            };

            channel.basicConsume(RabMQServer.RPC_QUEUE_NAME, false, consumer);

            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException _ignore) {
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (IOException _ignore) {}
        }

    }

    @Override
    protected void sendNotification(Response response) {
        try {
            byte[] responseData = Serialization.serialize(response);
            channel.basicPublish("notifications", "info", null, responseData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
