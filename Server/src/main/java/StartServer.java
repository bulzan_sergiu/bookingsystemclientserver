/**
 * Created by sergiubulzan on 14/05/2017.
 */
public class StartServer {
    public static void main(String[] args){
        IServer server = new TCPServer();
        server.start();
    }
}
