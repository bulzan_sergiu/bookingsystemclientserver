/**
 * Created by sergiubulzan on 14/05/2017.
 */
public interface IServer {
    void start();
}
