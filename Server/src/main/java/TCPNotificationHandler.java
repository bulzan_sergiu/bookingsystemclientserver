import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by sergiubulzan on 06/05/2017.
 */
public class TCPNotificationHandler {
    private Socket client;

    ObjectInputStream inStream;
    ObjectOutputStream outputStream;

    public boolean isConnected(){
        return client.isConnected();
    }

    public void startClient(Socket client){
        this.client = client;
    }

    public void notifyClients(Response response){
        if(client.isConnected()){
            try{
                inStream = new ObjectInputStream(client.getInputStream());
                outputStream = new ObjectOutputStream(client.getOutputStream());

                inStream.readObject();

                outputStream.writeObject(response);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
}
