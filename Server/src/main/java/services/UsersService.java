package services;

import exceptions.ExistingUserException;
import model.Users;
import repository.UserRepo;
import util.JdbcUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Observable;
import java.util.Properties;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class UsersService {
    UserRepo repo;

    public UsersService(UserRepo repo){
        this.repo = repo;
    }


    public void addUser(Users user) throws ExistingUserException {
        Users found = findByUsername(user.getUsername());
        if (found == null) {
            this.repo.save(user);
        } else {
            throw new ExistingUserException("User existent");
        }
    }


    public Users findUser(int Id){
        return this.repo.findOne(Id);
    }

    public Users findByUsername(String username){
        return this.repo.findUserName(username);
    }

    public List<Users> getAll() {
        return repo.findAll();
    }
}
