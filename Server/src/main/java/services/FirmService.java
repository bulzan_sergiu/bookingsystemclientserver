package services;

import model.Firm;
import repository.IRepository;

import java.util.List;
import java.util.Observable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class FirmService {
    IRepository<Integer, Firm> repo;

    public FirmService(IRepository<Integer, Firm> repo){
        this.repo = repo;
    }

    public void addFirm(Firm firm){
        this.repo.save(firm);
    }

    public void deleteFirm(int firmId){
        this.repo.delete(firmId);
    }

    public void updateFirm(int f1Id, Firm f2){
        this.repo.update(f1Id,f2);
    }

    public Firm findFirm(int Id){
        return repo.findOne(Id);
    }

    public List<Firm> getAll() {
        return repo.findAll();
    }
}
