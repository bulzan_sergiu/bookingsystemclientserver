package services;

import model.Excursion;
import repository.IRepository;

import java.util.List;
import java.util.Observable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ExcursionService {
    IRepository<Integer, Excursion> repo;

    public ExcursionService(IRepository<Integer, Excursion> repo){
        this.repo = repo;
    }

    public void addExcursion(Excursion excursion){
        this.repo.save(excursion);

    }

    public void deleteExcursion(int id){
        this.repo.delete(id);
    }

    public void updateExcursion(int ex1Id, Excursion ex2){
        this.repo.update(ex1Id, ex2);
    }

    public Excursion findExcursion(int Id){
        return this.repo.findOne(Id);
    }

    public List<Excursion> getAll() {
        return repo.findAll();
    }
}
