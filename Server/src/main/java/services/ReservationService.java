package services;

import model.Reservation;
import repository.IRepository;
import repository.ReservationHBNRepo;
import repository.ReservationRepo;

import java.util.List;
import java.util.Observable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class ReservationService {
    IRepository<Integer, Reservation> repo;

    public ReservationService(ReservationHBNRepo repo){
        this.repo = repo;
    }

    public void reserve(Reservation reservation){
        this.repo.save(reservation);
    }

    public void deleteReservation(int id){
        this.repo.delete(id);
    }

    public void updateReservation(int resvId, Reservation r2){
        this.repo.update(resvId, r2);
    }

    public Reservation findReservation(int Id){
        return this.repo.findOne(Id);
    }

    public List<Reservation> getAll() {
        return repo.findAll();
    }

    public Integer count(){
        return this.repo.size();
    }
}

