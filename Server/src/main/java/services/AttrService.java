package services;

import model.Attraction;
import repository.IRepository;

import java.util.List;
import java.util.Observable;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class AttrService{
    IRepository<Integer, Attraction> repo;

    public AttrService(IRepository<Integer, Attraction> repo){
        this.repo = repo;
    }

    public void addAttraction(Attraction attraction){
        this.repo.save(attraction);
    }

    public void deleteAttraction(int id){
        this.repo.delete(id);
    }

    public void updateAttraction(int a1Id, Attraction a2){
        this.repo.update(a1Id, a2);
    }

    public Attraction findAttraction(int Id){
        return this.repo.findOne(Id);
    }

    public List<Attraction> getAll() {
        return repo.findAll();
    }
}

