import repository.ExcursionRepo;
import repository.ReservationHBNRepo;
import repository.UserRepo;
import services.ExcursionService;
import services.ReservationService;
import services.UsersService;
import util.JdbcUtils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sergiubulzan on 08/04/2017.
 */
public class TCPServer implements IServer{

    UsersService usersService;
    ReservationService reservationService;
    ExcursionService excursionService;

    private final int port = 1234;

    public TCPServer() {
        Properties props = JdbcUtils.getProps();
        usersService = new UsersService(new UserRepo(props));
        reservationService = new ReservationService(new ReservationHBNRepo());
        excursionService = new ExcursionService(new ExcursionRepo(props));
    }

    List<TCPNotificationHandler> handlers = new ArrayList<>();

    public void start() {
        ExecutorService executor = null;
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            executor = Executors.newFixedThreadPool(5);
            System.out.println("Waiting for clients");
            while (true) {
                Socket clientSocket = serverSocket.accept();
                Socket notifierSocket = serverSocket.accept();

                TCPRequestHandler worker = new TCPRequestHandler(clientSocket, usersService, reservationService, excursionService, this);
                TCPNotificationHandler notifier = new TCPNotificationHandler();
                notifier.startClient(notifierSocket);

                executor.execute(worker);
                this.handlers.add(notifier);
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                    + port + " or listening for a connection");
            System.out.println(e.getMessage());
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void shouldUpdate(Response response) {
        for (TCPNotificationHandler handler : handlers) {
            if (handler.isConnected())
                handler.notifyClients(response);
        }
    }
}