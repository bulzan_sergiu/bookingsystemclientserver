import com.google.gson.Gson;
import com.rabbitmq.client.*;
import repository.ExcursionRepo;
import repository.ReservationHBNRepo;
import repository.UserRepo;
import services.ExcursionService;
import services.ReservationService;
import services.UsersService;
import util.JdbcUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by sergiubulzan on 13/05/2017.
 */
public class RabMQServer implements IServer {
    public static final String RPC_QUEUE_NAME = "bs_rpc_queue";

    static Gson gson = new Gson();
    static UsersService usersService = new UsersService(new UserRepo(JdbcUtils.getProps()));
    static ExcursionService excursionService = new ExcursionService(new ExcursionRepo(JdbcUtils.getProps()));
    static ReservationService reservationService = new ReservationService(new ReservationHBNRepo());

    static Channel channel;

    public RabMQServer(){    }

    public void start(){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        Connection connection = null;
        try{
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(RPC_QUEUE_NAME, false,false,false,null);
            channel.basicQos(1);
            System.out.println(" [x] Awaiting RPC requests");

            channel.exchangeDeclare("notifications", BuiltinExchangeType.DIRECT);
            String severity = "info";

            RabMQRequestHandler handler = new RabMQRequestHandler(usersService,reservationService,excursionService, channel,connection);
            new Thread(handler).start();

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
